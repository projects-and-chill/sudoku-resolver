# SELECT & EDIT SUDOKU :wrench:

In file : `./src/sudokus` you can :

- **Edit/Add sudoku** in `sudokus` variable
- **Select sudoku to use** in `SUDOKU_SELECTED` variable

# EXECUTE RESOLVER :video_game:



Install node_modules
```cmd
npm i
```

Run command
```cmd
npm run resolve
```
