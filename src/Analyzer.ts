import Table from "cli-table";
import {ISudoku} from "./interface/ISudoku";
import {CelluleInfos, ColumnInfos, RowInfos, SquareInfos} from "./Entities";
import {ICellInfos, IColInfos, IRowInfos, ISquareInfos} from "./interface/IEntitiesInfos";
import {numbers} from "./interface/constantes";
import {IAnalyzer} from "./interface/IAnalyzer";


export class Analyzer implements IAnalyzer{

  cellulesInfos: Record<number, ICellInfos>
  colsInfos: Record<number, IColInfos>
  rowsInfos: Record<number, IRowInfos>
  squaresInfos: Record<number, ISquareInfos>

  constructor(public incompletSudoku :ISudoku ) {
    this.cellulesInfos  = this.getCellsInfos()
    this.rowsInfos  = this.getRowsInfos(Object.values(this.cellulesInfos))
    this.colsInfos  = this.getColumnInfos(Object.values(this.cellulesInfos))
    this.squaresInfos = this.getSquareInfos(Object.values(this.cellulesInfos))
  }

  printIncompleteSudoku(){
    const tableIncompletSudoku = new Table({  style: { 'padding-left': 3, 'padding-right': 3 }})
    this.incompletSudoku.forEach(row => tableIncompletSudoku.push(row.map(val=> val ? val : '_')) )
    console.log('Sudoku de départ, nombre de cases inconues',this.getRemainingUnkownCells())
    console.log(tableIncompletSudoku.toString())
  }
  printCompleteSudoku(){
    const tableCompletSudoku = new Table({  style: { 'padding-left': 3, 'padding-right': 3 }})
    const completeSudoku = Object.values(this.rowsInfos).map(row=> row.cells.map(cell => cell.value))
    completeSudoku.forEach(row => tableCompletSudoku.push(row.map(val=> val ? val : '_')) )
    console.log(tableCompletSudoku.toString())
  }


  getRemainingUnkownCells = () : number => {
    let unknowValues = 0
    Object.values(this.cellulesInfos).forEach(cell => {
      if(cell.value === null)
        unknowValues++
    })
    return unknowValues
  }

  private getCellsInfos = () : Record<number, ICellInfos> => {
    let recordCells :  Record<number, ICellInfos> = {}
    this.incompletSudoku.forEach((row,rowIndex) => {
      const rowId = rowIndex+1

      row.forEach((val,colIndex)=> {

        const columnId = colIndex +1
        const cellInfos = new CelluleInfos(rowId,columnId, val)
        recordCells[cellInfos.cellId] = cellInfos
      })
    })
    return recordCells
  }

  private getRowsInfos = (cellsInfos : ICellInfos[]) : Record<number, RowInfos> => {
    const rowsInfos : Record<number, RowInfos> = {}
    numbers.forEach(rowId=> {
      rowsInfos[rowId] = new RowInfos(rowId, cellsInfos)
    })
    return rowsInfos
  }

  private getColumnInfos = (cellsInfos : ICellInfos[]) : Record<number, ColumnInfos> => {
    const colsInfos : Record<number, ColumnInfos> = {}
    numbers.forEach(colId=> {
      colsInfos[colId] = new ColumnInfos(colId, cellsInfos)
    })
    return colsInfos
  }

  private getSquareInfos = (cellsInfos : ICellInfos[]) : Record<number, SquareInfos> => {
    const squaresInfos : Record<number, SquareInfos> = {}
    numbers.forEach(squareId=> {
      squaresInfos[squareId] = new SquareInfos(squareId, cellsInfos)
    })
    return squaresInfos
  }


}