import {ICellInfos, IColInfos, IRowInfos, ISquareInfos} from "./interface/IEntitiesInfos";
import {IPossibleValues} from "./interface/IPossibleValues";


export class FillLastCelluleFromGroup {

  constructor(
    private cellsInfos : Record<number, ICellInfos>,
    private rowsInfos : Record<number, IRowInfos>,
    private colsInfos: Record<number, IColInfos>,
    private squaresInfos:Record<number, ISquareInfos>,
  ) {

  }

  exec(){
    this.fillLastColunmsCell()
    this.fillLastRowsCell()
    this.filllastSquareCell()
  }


  private fillLastRowsCell(){
    for(const i in this.rowsInfos){
      const rowInfos = this.rowsInfos[i]
      const ownedValues = rowInfos.ownedValues
      if (ownedValues.length === 8){
        const missingValue : IPossibleValues = rowInfos.missingValues[0]
        const cell : ICellInfos = rowInfos.cells.filter(cell=> cell.value === null)[0]
        cell.value = missingValue
        cell.possibleValues = [missingValue]
        this.colsInfos[cell.columnId].ownedValues.push(missingValue)
        this.squaresInfos[cell.squareId].ownedValues.push(missingValue)
      }
    }
  }

  private fillLastColunmsCell(){
    for(const i in this.colsInfos){
      const colInfos = this.colsInfos[i]
      const ownedValues = colInfos.ownedValues
      if (ownedValues.length === 8){
        const missingValue : IPossibleValues = colInfos.missingValues[0]
        const cell : ICellInfos = colInfos.cells.filter(cell=> cell.value === null)[0]
        cell.value = missingValue
        cell.possibleValues = [missingValue]
        this.rowsInfos[cell.rowId].ownedValues.push(missingValue)
        this.squaresInfos[cell.squareId].ownedValues.push(missingValue)
      }
    }
  }
  private filllastSquareCell(){
    for(const i in this.squaresInfos){
      const squareInfos = this.squaresInfos[i]
      const ownedValues = squareInfos.ownedValues
      if (ownedValues.length === 8){
        const missingValue : IPossibleValues = squareInfos.missingValues[0]
        const cell : ICellInfos = squareInfos.cells.filter(cell=> cell.value === null)[0]
        cell.value = missingValue
        cell.possibleValues = [missingValue]
        this.rowsInfos[cell.rowId].ownedValues.push(missingValue)
        this.colsInfos[cell.columnId].ownedValues.push(missingValue)
      }
    }
  }

}




