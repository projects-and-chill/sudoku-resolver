import {ICellInfos, IColInfos, IRowInfos, ISquareInfos} from "./interface/IEntitiesInfos";


export class FillByPossibilitiesElimination {

  constructor(
    private cellsInfos : Record<number, ICellInfos>,
    private rowsInfos : Record<number, IRowInfos>,
    private colsInfos: Record<number, IColInfos>,
    private squareInfos:Record<number, ISquareInfos>,
  ) {

  }

  exec(){
    for(const i in this.cellsInfos){
      const cell = this.cellsInfos[i]
      this.removeImpossibleValuesCols(cell)
      this.removeImpossibleValuesRows(cell)
      this.removeImpossibleValuesSquare(cell)

      if (cell.possibleValues.length === 1 && cell.value === null){
        cell.value = cell.possibleValues[0]
        this.rowsInfos[cell.rowId].ownedValues.push(cell.value)
        this.colsInfos[cell.columnId].ownedValues.push(cell.value)
      }

      else if(cell.possibleValues.length === 0 && cell.value === null)
        throw `ATTENTION : Fck Sudoku impossible ! à la cellule ${cell.cellId} (case ${cell.squareId})`

    }
  }


  private removeImpossibleValuesRows(cellInfos : ICellInfos){
    const rowId = cellInfos.rowId
    const rowInfos : IRowInfos = this.rowsInfos[rowId]
    cellInfos.possibleValues = cellInfos.possibleValues.filter(possVal => !rowInfos.ownedValues.includes(possVal))

    rowInfos.theoryOccuped.forEach(theoryObj => {
      if( cellInfos.squareId !== theoryObj.skipSquare )
        cellInfos.possibleValues = cellInfos.possibleValues.filter(possVal => possVal !== theoryObj.value)
    })
  }

  private removeImpossibleValuesCols(cellInfos : ICellInfos){
    const colId = cellInfos.columnId
    const colInfos : IColInfos = this.colsInfos[colId]
    cellInfos.possibleValues = cellInfos.possibleValues.filter(possible => !colInfos.ownedValues.includes(possible))

    colInfos.theoryOccuped.forEach(theoryObj => {
      if( cellInfos.squareId !== theoryObj.skipSquare )
        cellInfos.possibleValues = cellInfos.possibleValues.filter(possVal => possVal !== theoryObj.value)
    })
  }

  private removeImpossibleValuesSquare(cellInfos : ICellInfos){
    const squareId = cellInfos.squareId
    const squareInfos : ISquareInfos = this.squareInfos[squareId]
    cellInfos.possibleValues = cellInfos.possibleValues.filter(possible => !squareInfos.ownedValues.includes(possible))

  }

}



