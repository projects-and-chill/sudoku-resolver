import {IPossibleValues} from "./IPossibleValues";

interface IEntityInfo {
  ownedValues : IPossibleValues[],
  missingValues : IPossibleValues[]
  cells : ICellInfos[]
}

export interface IRowInfos extends IEntityInfo {
  rowId : number
  theoryOccuped : {value : IPossibleValues, skipSquare : number }[]
}

export interface IColInfos extends IEntityInfo{
  colId : number
  theoryOccuped : {value : IPossibleValues, skipSquare : number }[]
}

export interface ISquareInfos extends IEntityInfo{
  squareId : number
}

export interface ICellInfos {
  cellId : number
  rowId: number
  columnId:number
  squareId:number
  possibleValues : IPossibleValues[]
  value : null|IPossibleValues
}