import {IPossibleValues} from "./IPossibleValues";

export const sudokuWidth = 9
export const SquareWidth = 3
export const maxTry = 100
export const numbers: IPossibleValues[] = [1,2,3,4,5,6,7,8,9]