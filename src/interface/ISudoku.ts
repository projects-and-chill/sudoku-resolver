import {IPossibleValues} from "./IPossibleValues";


type ICase = IPossibleValues | null
type ILigne = [ICase,ICase,ICase,ICase,ICase,ICase,ICase,ICase,ICase]

export type ISudoku = [
  ILigne,
  ILigne,
  ILigne,
  ILigne,
  ILigne,
  ILigne,
  ILigne,
  ILigne,
  ILigne,
]
