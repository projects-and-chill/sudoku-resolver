import {ICellInfos, IColInfos, IRowInfos, ISquareInfos} from "./IEntitiesInfos";
import {ISudoku} from "./ISudoku";

export interface IAnalyzer {
  rowsInfos : Record<number, IRowInfos>
  colsInfos: Record<number, IColInfos>
  squaresInfos : Record<number, ISquareInfos>
  cellulesInfos: Record<number, ICellInfos>
  incompletSudoku : ISudoku
}