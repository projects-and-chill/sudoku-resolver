import {ICellInfos, IColInfos, IRowInfos, ISquareInfos} from "./interface/IEntitiesInfos";
import {IPossibleValues} from "./interface/IPossibleValues";
import {numbers, SquareWidth, sudokuWidth} from "./interface/constantes";

export class ColumnInfos implements IColInfos {

  cells: ICellInfos[]
  theoryOccuped : {value : IPossibleValues, skipSquare : number }[] = []

  constructor(public colId: number, cellsInfos: ICellInfos[],) {
    this.cells = cellsInfos.filter(cell => cell.columnId === colId)
  }

  get missingValues() : IPossibleValues[] {
    return numbers.filter(num => !this.ownedValues.includes(num))
  }

  get ownedValues() : IPossibleValues[] {
    return this.cells.filter(cell => cell.value !== null).map(cell=> cell.value) as IPossibleValues[]
  }

}

export class RowInfos implements IRowInfos {

  cells: ICellInfos[]
  theoryOccuped : {value : IPossibleValues, skipSquare : number }[] = []

  constructor(public rowId: number, cellsInfos: ICellInfos[],) {
    this.cells = cellsInfos.filter(cell => cell.rowId === rowId)
  }

  get missingValues() : IPossibleValues[] {
    return numbers.filter(num => !this.ownedValues.includes(num))
  }

  get ownedValues() : IPossibleValues[] {
    return this.cells.filter(cell => cell.value !== null).map(cell=> cell.value) as IPossibleValues[]
  }

}

export class SquareInfos implements ISquareInfos {


  cells: ICellInfos[]

  constructor(public squareId: number, cellsInfos: ICellInfos[]) {
    this.cells = cellsInfos.filter(cell => cell.squareId === squareId)
  }

  get missingValues() : IPossibleValues[] {
    return numbers.filter(num => !this.ownedValues.includes(num))
  }

  get ownedValues() : IPossibleValues[] {
    return this.cells.filter(cell => cell.value !== null).map(cell=> cell.value) as IPossibleValues[]
  }

}

export class CelluleInfos implements ICellInfos {



  public possibleValues : IPossibleValues[] = [...numbers]
  public squareId:number
  public cellId : number
  private _value : null|IPossibleValues

  constructor(
    public rowId: number,
    public columnId:number,
    value
) {
    this._value = value
    this.squareId = this.getSquareId(rowId,columnId)
    this.cellId = columnId + (rowId-1) * sudokuWidth

  }


  private getSquareId = (minorRow:number,minorCol:number) => {
    const qtyMajorCol = sudokuWidth / SquareWidth

    const squareX = Math.floor((minorCol-1) / SquareWidth)
    const squareY = Math.floor((minorRow-1) / SquareWidth)
    return (squareY*qtyMajorCol)+squareX+1
  }

  set value (val:IPossibleValues|null){
    if(val !== null && !this.possibleValues.includes(val))
      throw  `Vous essayez de donner une valeur interdite à la case --${this.cellId}-- !`
    this._value = val
  }

  get value (){
    return this._value
  }


}