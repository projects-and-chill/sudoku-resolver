import {ICellInfos, IColInfos, IRowInfos, ISquareInfos} from "./interface/IEntitiesInfos";

export class FillByTheorericallyOccupation {

  constructor(
    private rowsInfos : Record<number, IRowInfos>,
    private colsInfos: Record<number, IColInfos>,
    private squareInfos:Record<number, ISquareInfos>,
  ) {

  }

  exec(){
    for(const i in this.squareInfos){
      const square = this.squareInfos[i]
      this.makeTheorySquareOccupation(square)
    }
  }


  private makeTheorySquareOccupation(square : ISquareInfos){
    const missingValues = square.missingValues

    const possibilities : ICellInfos[] = square.cells.filter(cell => cell.possibleValues.some(p => missingValues.includes(p)) )

    missingValues.forEach(missVal => {
      const sortedCells = [...possibilities.filter(p => p.possibleValues.includes(missVal) )]
      if(sortedCells.length >= 2 && sortedCells.every(cell => cell.rowId === sortedCells[0].rowId )){
        this.rowsInfos[sortedCells[0].rowId].theoryOccuped.push({
          value : missVal,
          skipSquare : sortedCells[0].squareId
        })
      }
      else if(sortedCells.length >= 2 && sortedCells.every(cell => cell.columnId === sortedCells[0].columnId )){
        this.colsInfos[sortedCells[0].columnId].theoryOccuped.push({
          value : missVal,
          skipSquare : sortedCells[0].squareId
        })
      }

    })

  }

}



