import {ICellInfos, IColInfos, IRowInfos, ISquareInfos} from "./interface/IEntitiesInfos";
import {IPossibleValues} from "./interface/IPossibleValues";


export class FillByPossibilitiesComparaison {

  constructor(
    private cellsInfos : Record<number, ICellInfos>,
    private rowsInfos : Record<number, IRowInfos>,
    private colsInfos: Record<number, IColInfos>,
    private squaresInfos:Record<number, ISquareInfos>,
  ) {

  }

  exec(){
    Object.values( this.rowsInfos).forEach(row=>{
      this.fillRow(row)
    })
    Object.values( this.colsInfos).forEach(col=>{
      this.fillColumn(col)
    })
    Object.values( this.squaresInfos).forEach(square=>{
      this.fillSquare(square)
    })
  }


  private fillRow(row : IRowInfos){
    const missingValues : IPossibleValues[] = row.missingValues
    const unfillCells : ICellInfos[] = row.cells.filter(cell => cell.value === null)

    missingValues.forEach(missVal => {
      const validCells : ICellInfos[] = []
      for(const unfillCell of unfillCells){
        if(unfillCell.possibleValues.includes(missVal))
          validCells.push(unfillCell)
      }
      if(validCells.length === 1){
        const cell = validCells[0]
        cell.value = missVal
        cell.possibleValues = [missVal]
        this.colsInfos[cell.columnId].ownedValues.push(missVal)
        this.squaresInfos[cell.squareId].ownedValues.push(missVal)
      }
    })
  }

  private fillColumn(col : IColInfos){
    const missingValues :IPossibleValues[] = col.missingValues
    const unfillCells : ICellInfos[] = col.cells.filter(cell => cell.value === null)

    missingValues.forEach(missVal => {
      const validCells : ICellInfos[] = []
      for(const unfillCell of unfillCells){
        if(unfillCell.possibleValues.includes(missVal))
          validCells.push(unfillCell)
      }
      if(validCells.length === 1){
        const cell = validCells[0]
        cell.value = missVal
        cell.possibleValues = [missVal]
        this.rowsInfos[cell.rowId].ownedValues.push(missVal)
        this.squaresInfos[cell.squareId].ownedValues.push(missVal)
      }
    })
  }

  private fillSquare(square : ISquareInfos){
    const missingValues : IPossibleValues[] = square.missingValues
    const unfillCells : ICellInfos[] = square.cells.filter(cell => cell.value === null)

    missingValues.forEach(missVal => {
      const validCells : ICellInfos[] = []
      for(const unfillCell of unfillCells){
        if(unfillCell.possibleValues.includes(missVal))
          validCells.push(unfillCell)
      }
      if(validCells.length === 1){
        const cell = validCells[0]
        cell.value = missVal
        cell.possibleValues = [missVal]
        this.colsInfos[cell.columnId].ownedValues.push(missVal)
        this.rowsInfos[cell.rowId].ownedValues.push(missVal)
      }
    })
  }

}

