import {ISudoku} from "./interface/ISudoku";

const sudokus : Record<string, ISudoku> = {
  FACILE_1 : [//FACILE
    [1,9,null,null,null,6,null,null,2],
    [null,null,null,null,null,2,7,1,3],
    [2,null,3,null,8,4,null,5,null],
    [7,5,null,6,null,null,1,null,null],
    [null,8,null,5,2,1,null,6,null],
    [null,null,6,null,null,7,null,2,9],
    [null,3,null,2,6,null,4,null,1],
    [6,4,1,3,null,null,null,null,null],
    [9,null,null,4,null,null,null,3,5],
  ],

  MOYEN_1 : [
    [null,null,null,null,null,null,null,null,null],
    [null,null,null,null,null,null,null,null,null],
    [null,null,null,null,null,null,null,null,null],
    [null,null,null,null,null,null,null,null,null],
    [null,null,null,null,null,null,null,null,null],
    [null,null,null,null,null,null,null,null,null],
    [null,null,null,null,null,null,null,null,null],
    [null,null,null,null,null,null,null,null,null],
    [null,null,null,null,null,null,null,null,null],
  ],
  DIFFICILE_1 : [
    [null,null,3,null,2,null,null,null,6],
    [null,1,null,null,null,4,null,null,null],
    [null,null,7,9,null,null,2,1,5],
    [6,null,null,null,null,7,null,2,4],
    [null,null,null,null,null,null,null,null,null],
    [1,7,null,8,null,null,null,null,9],
    [2,5,8,null,null,9,3,null,null],
    [null,null,null,5,null,null,null,7,null],
    [7,null,null,null,6,null,5,null,null],
  ],
  EXTREME_1 : [
    [null,5,null,null,null,null,null,null,null],
    [8,null,null,null,3,null,5,6,null],
    [null,4,6,1,null,null,8,null,3],
    [7,3,null,null,1,null,null,null,null],
    [null,null,null,8,null,7,null,null,null],
    [null,null,null,null,9,null,null,2,8],
    [9,null,4,null,null,1,2,3,null],
    [null,7,1,null,4,null,null,null,9],
    [null,null,null,null,null,null,null,7,null]
  ]
}


export const SUDOKU_SELECTED = sudokus.EXTREME_1

