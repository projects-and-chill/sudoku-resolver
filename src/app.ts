
import {FillByPossibilitiesElimination} from "./FillByPossibilitiesElimination";
import {FillByPossibilitiesComparaison} from "./FillByPossibilitiesComparaison";
import {FillLastCelluleFromGroup} from "./FillLastCelluleFromGroup";
import {IAnalyzer} from "./interface/IAnalyzer";
import {Analyzer} from "./Analyzer";
import {maxTry} from "./interface/constantes";
import {SUDOKU_SELECTED} from "./sudokus";
import {FillByTheorericallyOccupation} from "./FillByTheorericallyOccupation";



const analizer = new Analyzer(SUDOKU_SELECTED)
const launch = (analyser : IAnalyzer)=>{
  new  FillByPossibilitiesElimination(
    analyser.cellulesInfos,
    analyser.rowsInfos,
    analyser.colsInfos,
    analyser.squaresInfos
  ).exec()
  new FillByPossibilitiesComparaison(
    analyser.cellulesInfos,
    analyser.rowsInfos,
    analyser.colsInfos,
    analyser.squaresInfos
  ).exec()
  new FillLastCelluleFromGroup(
    analyser.cellulesInfos,
    analyser.rowsInfos,
    analyser.colsInfos,
    analyser.squaresInfos
  ).exec()
  new FillByTheorericallyOccupation(
    analyser.rowsInfos,
    analyser.colsInfos,
    analyser.squaresInfos
  ).exec()
}


analizer.printIncompleteSudoku()

let shots = 0
let stuckTimes = 0
let unkownCellsCount = analizer.getRemainingUnkownCells()

try{
  while( unkownCellsCount && shots<=maxTry ){
    shots++
    launch(analizer)
    const newUnkownCellsCount = analizer.getRemainingUnkownCells()

    newUnkownCellsCount === unkownCellsCount ? stuckTimes++ : stuckTimes = 0

    if(stuckTimes > 2)
       throw "je suis bloqué ! :'("

    unkownCellsCount = newUnkownCellsCount
  }
}
catch (msg){
  console.log(msg)
}


console.log("Nombre de tentatives", shots)
console.log("Nombre de case inconnues", unkownCellsCount)
analizer.printCompleteSudoku()
